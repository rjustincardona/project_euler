import Data.List

-- factors n = [x | x <- [2..floor (sqrt (fromIntegral n))], (mod n x) == 0]
factors n = [x | x <- [2..n], (mod n x) == 0]


main :: IO ()
main = print (factors 600851475143)