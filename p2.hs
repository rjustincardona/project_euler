fibs = 1 : 2 : zipWith (+) fibs (tail fibs)

main :: IO ()
main = print (sum (filter (\x -> mod x 2 == 0) ((takeWhile (<= 4_000_000)) fibs)))